function FindProxyForURL(url, host)
{
	var myip = myIpAddress();  // Set a variable for the local IP address.

	if ((isInNet(myip, "172.20.101.0", "255.255.255.0")) || 
		(isPlainHostName(host)) || 
		(shExpMatch(host, "*.ncta.com")) || 
		(shExpMatch(host, "*.ciconline.org")) || 
		(shExpMatch(host, "*.walterkaitz.org")) || 
		(shExpMatch(host, "*.thecableshow.com")) ||
		(shExpMatch(host, "streaming.broadband.gov")) ||
		(shExpMatch(host, "ld.congress.gov"))) {
	return "DIRECT"; }
	else {
	return "PROXY 172.26.0.88:8080";
	}
}
