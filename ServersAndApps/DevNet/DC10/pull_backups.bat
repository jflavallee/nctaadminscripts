robocopy \\edessa\sqlbackup c:\backup\edessa /mir /COPY:DT /NP /LOG:c:\backup\logs\EDESSA_sql.txt /TEE
robocopy \\cicdev\tomcat c:\backup\cicdev\tomcat /mir /NP /COPY:DT /LOG:c:\backup\logs\cicdev_tomcat.txt /TEE
robocopy \\cicdev\websites c:\backup\cicdev\websites /mir /NP /COPY:DT /NP /LOG:c:\backup\logs\cicdev_websites.txt /TEE
robocopy \\nctadev\livesites c:\backup\nctadev\livesites /mir /NP /COPY:DT /NP /LOG:c:\backup\logs\nctadev_livesites.txt /TEE