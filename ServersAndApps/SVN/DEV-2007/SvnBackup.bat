REM Cleanup
del d:\svnstuff\backup\*.* /F /S /q
rd d:\svnstuff\backup\CableShowSite /s /q
rd d:\svnstuff\backup\MergePurge /s /q
rd d:\svnstuff\backup\Peeps /s /q
rd d:\svnstuff\backup\RemoteAccessSite /s /q
rd d:\svnstuff\backup\Cte2008 /s /q
rd d:\svnstuff\backup\ControlYourTV /s /q
rd d:\svnstuff\backup\NctaAdminScripts /s /q
rd d:\svnstuff\backup\CiCOnline /s /q
rd d:\svnstuff\backup\PIAS /s /q
rd d:\svnstuff\backup\NCTAWebsite /s /q
rd d:\svnstuff\backup\CicApps /s /q
REM copy config
copy d:\svnstuff\Repositories\authz d:\svnstuff\backup
copy d:\svnstuff\Repositories\htpasswd d:\svnstuff\backup
copy d:\svnstuff\Repositories\server.log d:\svnstuff\backup
copy d:\svnstuff\Repositories\server.pid d:\svnstuff\backup
copy d:\svnstuff\Repositories\ssl_scache.dir d:\svnstuff\backup
copy d:\svnstuff\Repositories\ssl_scache.pag d:\svnstuff\backup
REM backup repositories
svnadmin hotcopy d:\svnstuff\repositories\CableShowSite d:\svnstuff\backup\CableShowSite
svnadmin hotcopy d:\svnstuff\repositories\MergePurge d:\svnstuff\backup\MergePurge
svnadmin hotcopy d:\svnstuff\repositories\Peeps d:\svnstuff\backup\Peeps
svnadmin hotcopy d:\svnstuff\repositories\RemoteAccessSite d:\svnstuff\backup\RemoteAccessSite
svnadmin hotcopy d:\svnstuff\repositories\Cte2008 d:\svnstuff\backup\Cte2008
svnadmin hotcopy d:\svnstuff\repositories\ControlYourTV d:\svnstuff\backup\ControlYourTV
svnadmin hotcopy d:\svnstuff\repositories\NctaAdminScripts d:\svnstuff\backup\NctaAdminScripts
svnadmin hotcopy d:\svnstuff\repositories\CiCOnline d:\svnstuff\backup\CiCOnline
svnadmin hotcopy d:\svnstuff\repositories\PIAS d:\svnstuff\backup\PIAS
svnadmin hotcopy d:\svnstuff\repositories\NCTAWebsite d:\svnstuff\backup\NCTAWebsite
svnadmin hotcopy d:\svnstuff\repositories\CicApps d:\svnstuff\backup\CicApps