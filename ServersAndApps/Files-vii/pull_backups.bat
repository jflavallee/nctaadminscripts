REM INET
robocopy \\inet\backup d:\backup\inet\sharepoint /s /purge /NP /LOG:D:\backup\logs\inet-sharepoint.txt
robocopy \\inet\webapps$ d:\backup\inet\webapps /s /purge /NP /LOG:D:\backup\logs\inet-webapps.txt

REM BES
robocopy \\bes-2k-0\backup d:\backup\bes /s /purge /NP /LOG:D:\backup\logs\bes-2k-0.txt

REM casper
robocopy \\casper\backup d:\backup\casper /s /purge /NP /LOG:D:\backup\logs\casper.txt

REM CCM
robocopy \\ccm1\backup d:\backup\ccm /s /purge /NP /LOG:D:\backup\logs\ccm.txt

REM CIC_Legacy_web
REM no mo web.ncta.com, this now runs under web09-vm
REM robocopy \\web\cic_ftp d:\backup\cic_legacy_web /s /purge /NP /LOG:D:\backup\logs\cic_legacy_web.txt

REM Unity
REM Unity uses NT Backup to push the backup to \\files-vii\unity$

REM DC10
robocopy \\DC10\backup d:\backup\DC10 /s /purge /NP /LOG:D:\backup\logs\dc10.txt

REM DC11
robocopy \\DC11\backup d:\backup\DC11 /s /purge /NP /LOG:D:\backup\logs\dc11.txt

REM DEV-2007
REM no longer needed, all this is inactive
REM robocopy \\dev-2007\SvnBackup d:\backup\dev-2007\svn /E /purge /NP /LOG:D:\backup\logs\dev-2007svn.txt
REM robocopy \\dev-2007\VssBackup d:\backup\dev-2007\vss /E /purge /NP /LOG:D:\backup\logs\dev-2007vss.txt

REM Filez
robocopy \\filez\backup$\sts d:\backup\filez\sts /s /purge /LOG:D:\backup\logs\sts.txt

REM w2k-vm-adp
robocopy \\w2k-vm-adp\pay4win$ d:\backup\w2k-vm-adp /s /purge /LOG:D:\backup\logs\w2k-vm-adp.txt

REM Gypsy
REM Don't NEED THIS
REM robocopy \\gypsy\backup d:\backup\gypsy /s /purge /NP /LOG:D:\backup\logs\gypsy.txt

REM EFES: SecretServer only
REM First, delete yesterday's file, then copy today's file.
d:
cd \backup\efes_SecretServer
erase *.bak /q
robocopy *.bak \\efes\t$\sql2005\backup\UserDBs\SecretServer d:\backup\efes_SecretServer\ /maxage:1 /NP /LOG:d:\backup\logs\efes_SecretServer.txt
REM EFES: SecretServer only
REM First, delete yesterday's file, then copy today's file.
REM erase d:\backup\efes\*.bak /q
REM robocopy \\efes\t$\sql2005\backup\UserDBs\SecretServer\ d:\backup\efes\ /maxage:1 /NP /LOG:c:\backup\logs\efes.txt

REM web09-vm pull
robocopy \\web09-vm\webdata$ d:\backup\web09-vm /s /purge /NP /LOG:D:\backup\logs\web09-vm.txt

REM seerweb pull
robocopy \\seerweb\LiveSites$ d:\backup\seerweb\LiveSites /s /purge /NP /LOG:D:\backup\logs\seerweb_livesites.txt
robocopy \\seerweb\Apps$ d:\backup\seerweb\Apps /s /purge /NP /LOG:D:\backup\logs\seerweb_apps.txt
robocopy \\seerweb\backup$ d:\backup\seerweb\Backup /s /purge /NP /LOG:D:\backup\logs\seerweb_backup.txt

REM seerdb pull
robocopy \\seerdb\backup$ d:\backup\seerdb /s /purge /NP /LOG:D:\backup\logs\seerdb.txt

REM SVN pull
robocopy \\svn\svn$ d:\backup\svn /s /purge /NP /LOG:D:\backup\logs\svn.txt