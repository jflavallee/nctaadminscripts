REM EFES: Nav4-5 only
REM First, delete yesterday's file, then copy today's file.
d:
cd \backup\efes_Nav4-5
erase *.bak /q

robocopy *.bak \\efes\t$\sql2005\backup\UserDBs\nav4-5 d:\backup\efes_nav4-5\ /maxage:1 /NP /LOG+:d:\backup\logs\efes_Nav4-5.txt