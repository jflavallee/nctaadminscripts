Name	Type	Data	Timestamp
(same as parent folder)	Mail Exchanger (MX)	[10]  mail.businessofcable.com.	static
(same as parent folder)	Start of Authority (SOA)	[21], dc-ix.ncta.com., admin.ncta.com.	static
www	Host (A)	172.31.5.13	static
mail	Host (A)	172.31.5.2	static
(same as parent folder)	Name Server (NS)	dc-ii.ncta.com.	static
(same as parent folder)	Name Server (NS)	dc-ix.ncta.com.	static
(same as parent folder)	Name Server (NS)	dc10.ncta.com.	static
(same as parent folder)	Name Server (NS)	dc11.ncta.com.	static
(same as parent folder)	Name Server (NS)	gypsy.ncta.com.	static
