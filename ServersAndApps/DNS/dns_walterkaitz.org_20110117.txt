Name	Type	Data	Timestamp
(same as parent folder)	Start of Authority (SOA)	[48], dc-ix.ncta.com., admin.ncta.com.	static
(same as parent folder)	Name Server (NS)	dc-ix.ncta.com.	static
(same as parent folder)	Name Server (NS)	dc11.ncta.com.	static
(same as parent folder)	Name Server (NS)	gypsy.ncta.com.	static
(same as parent folder)	Name Server (NS)	dc10.ncta.com.	static
(same as parent folder)	Name Server (NS)	dc-ii.ncta.com.	static
(same as parent folder)	Mail Exchanger (MX)	[10]  mail.walterkaitz.org.	static
*	Alias (CNAME)	www.walterkaitz.org.	static
careercenter	Alias (CNAME)	cname.boxwoodtech.com.	static
i	Host (A)	209.81.90.230	static
mail	Host (A)	172.31.5.2	static
nl	Host (A)	209.81.90.236	static
qa	Host (A)	66.240.5.202	static
www	Host (A)	209.81.90.235	static
