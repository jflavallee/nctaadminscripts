param(
	$url="http://localhost",
	$backupFolder="c:\"
)
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint")

$site= new-Object Microsoft.SharePoint.SPSite($url)
$names=$site.WebApplication.Sites.Names
foreach ($name in $names)
{
	$n2 = ""
	if ($name.Length -eq 0) 
		{ $n2="ROOT" }
	else
		{ $n2 = $name }
	$tmp=$n2.Replace("/", "_") + ".sbk"
	$saveas = ""
	if ($backupFolder.Length -eq 0) 
		{ $saveas = $tmp }
	else
		{ $saveas = join-path -path $backupFolder -childPath $tmp }
        $site.WebApplication.Sites.Backup($name, $saveas, "true")
        write-host "$n2 backed up to $saveas."
}