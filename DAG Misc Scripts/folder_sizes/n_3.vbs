on error resume next
dim objShell, objFSO, objCSV, objDrive, objRootFolder, colSubFolders, objSubFolder, colSubFolders2, objSubFolder2, colSubFolders3, objSubFolder3 
dim strDriveSize, strFolderSize, strFS, strFS2, strSizeCategory, strSizeCalc, strLine, strLine2, strLine3

set objShell = createobject("wscript.shell")
set objFSO = createobject("scripting.filesystemobject")

set objDrive = objFSO.GetDrive("n:")
strDriveSize = objDrive.TotalSize
Set objRootFolder = objDrive.RootFolder
Set colsubFolders = objRootFolder.Subfolders
	For Each objSubFolder In colSubFolders
	If objSubFolder.name <> temp then
		strFolderSize = objSubFolder.size
		Set objCSV = objFSO.OpenTextFile("c:\scripts_output\n_drive\" & objSubFolder.name & ".csv",2,True)
		strLine = "FolderName,FolderSize,x,Percentage,x,RealSize"
		objCSV.WriteLine strLine
		strLine = chr(34) & objSubfolder.path & chr(34) & "," & Sizer(strFolderSize) & "," & strSizeCategory & "," & Portion (strFolderSize)& ",%," & strFolderSize
		objCSV.WriteLine strLine
		Set colSubFolders2 = objSubFolder.Subfolders
		
		For Each objSubFolder2 In colSubFolders2
			wscript.echo objSubFolder2.path
			strFolderSize = objSubFolder2.size
			strLine2 = chr(34) & objSubfolder2.path & chr(34) & "," & Sizer(strFolderSize) & "," & strSizeCategory & "," & Portion (strFolderSize) & ",%," & strFolderSize
			objCSV.WriteLine strLine2
			Set colSubFolders3 = objSubFolder2.Subfolders

			For Each objSubFolder3 In colSubFolders3
				wscript.echo objSubFolder3.path
				strFolderSize = objSubFolder3.size
				strLine3 = chr(34) & objSubfolder3.path & chr(34) & "," & Sizer(strFolderSize) & "," & strSizeCategory & "," & Portion (strFolderSize) & ",%," & strFolderSize
				objCSV.WriteLine strLine3
			Next
		Next
	End If	
	Next
wscript.echo vbcrlf & "Finished!"	

Function Sizer(strFS)
If strFS < 1024 Then
strSizeCategory = "B"
Sizer = strFS
Else If strFS < 1048576 then
strSizeCategory = "KB"
Sizer = round(strFS/1024,2)
Else If strFS < 1073741824 Then
strSizeCategory = "MB"
Sizer = round(strFS/1048576,2)
Else
strSizeCategory = "GB"
Sizer = round(strFS/1073741824,2)
End If
End If
End If
End Function

Function Portion (strFS2)
Portion = Round (strFS2/strDriveSize*100,2)
End Function

