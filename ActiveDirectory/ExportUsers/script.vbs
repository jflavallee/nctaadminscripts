'* description: export users from Active Directory to a comma separated text file.
'*  Use the text file to create users in a new AD with the accompanying import script.
'*              
'* author: Chris Pilling
'* date: 18 June 2008
'* edit the attributes exported to suit
'* after running it is probably best to delete lines from the text file for system generated accounts
 
Const ForAppending = 8
 
Set objRoot = GetObject("LDAP://RootDSE") 
strDNC = objRoot.Get("DefaultNamingContext") 
Set objDomain = GetObject("LDAP://" & strDNC)
 
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.CreateTextFile("C:\scripts_output\AD_Users_Export.txt")

Call enummembers(objDomain)
 
Sub enumMembers(objDomain) 
On Error Resume Next 
 
For Each objMember In objDomain  
 
If ObjMember.Class = "user" Then
 
	If (isempty(objMember.EmployeeNumber)) Then
		EN =objMember.EmployeeNumber
		
		If Not (isempty(ObjMember.ipPhone)) Then
	 
			If Not (isempty(ObjMember.samAccountName)) Then 
				SamAccountName = ObjMember.samAccountName 
			else 
				SamAccountName = "" 
			End If

			If Not (isempty(ObjMember.CN)) Then Cn = ObjMember.CN else Cn = "" End If
			If Not (isempty(objMember.GivenName)) Then FirstName =objMember.GivenName else FirstName = "" End If
			If Not (isempty(objMember.sn)) Then Lastname = ObjMember.sn else LastName = "" End If
			If Not (isempty(objMember.UserPrincipalName)) Then 
				UserPrincipalName = objMember.UserPrincipalName 
			else 
				Name = "" 
			End If

			DID = objMember.ipPhone
			'If Not (isempty(objMember.EmployeeNumber)) Then EN =objMember.EmployeeNumber else EN = "" End If

			set objFolder = nothing
			set objFile = nothing
			 
			Set objTextFile = objFSO.OpenTextFile ("C:\scripts_output\AD_Users_Export.txt", ForAppending, True)
			 
			wscript.echo SamAccountName & "," & DID & "," & EN
			 
			strText1 = SamAccountName & "," & DID & "," & EN

			objTextFile.WriteLine(strText1) 
			objTextFile.Close
 
		End If

	End If 
	
End If
  
	If objMember.Class = "organizationalUnit" or OBjMember.Class = "container" Then 
	enumMembers (objMember) 
	End If 

Next 

End Sub