On Error Resume Next
Dim objShell, objNetwork, objUser, objCurrentUser, objLogonLogFile
Dim colGroupList,colVolatileEnvVars
Dim strGroup, strGroupPath, strAllGroups, strOutlookTag, strUserName, strLogonLogFile, strLocalLogonLogPath, strNetworkLogonLogPath
Dim strComputerName, strSite

Set objShell = WScript.CreateObject("WScript.Shell")
Set objNetwork = CreateObject("WScript.Network")
Set objUser = CreateObject("ADSystemInfo")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set colVolatileEnvVars = objShell.Environment("Volatile")

'Get Computer Name, User info and Group Membership
strComputerName = objNetwork.ComputerName
Set objCurrentUser = GetObject("LDAP://" & objUser.UserName)
strUserName = objCurrentUser.Get("name")

REM Record log off in Application Event Log
objShell.LogEvent 0,"User " & strUserName & " is logging off..."

'This next section checks the machines IP address to determine if it is offsite (i.e., at the Cable Show)
strSite="ncta"
strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")

Set IPConfigSet = objWMIService.ExecQuery _
    ("Select * from Win32_NetworkAdapterConfiguration Where IPEnabled=TRUE")
 
For Each IPConfig in IPConfigSet
    If Not IsNull(IPConfig.IPAddress) Then 
        For i=LBound(IPConfig.IPAddress) to UBound(IPConfig.IPAddress)
        If instr(IPConfig.IPAddress(i), "172.27") then
	strSite="remote"
	End If
        Next
    End If
Next

'Map all normal drives 
'If drives are already mapped, error results--this shouldn't be a problem unless someone
'has mapped these drives to other shares, or we move the shares.
'objNetwork.MapNetworkDrive "G:" , "\\filez\shared"
'objNetwork.MapNetworkDrive "N:" , "\\filez\data"
'objNetwork.MapNetworkDrive "S:" , "\\andorra\scanners"

'Get Computer Name, User info and Group Membership
strComputerName = objNetwork.ComputerName
Set objCurrentUser = GetObject("LDAP://" & objUser.UserName)
strUserName = objCurrentUser.Get("name")
colGroupList = objCurrentUser.GetEx("MemberOf")

'Logon Log
If strSite="ncta" then
strNetworkLogonLogPath = "\\dc-ii\logon_logs\"
strLogonLogFile = strUserName & "_On_" & strComputerName & ".log"
If objFSO.FileExists(strNetworkLogonLogPath & strLogonLogFile) Then
'no action needed
	Else Set objLogonLogFile = objFSO.CreateTextFile(strNetworkLogonLogPath & strLogonLogFile)
End If
Set objLogonLogFile = objFSO.OpenTextFile(strNetworkLogonLogPath & strLogonLogFile, 8)
objLogonLogFile.WriteLine "Out, " & date & ", " & time
objLogonLogFile.close
objFSO.DeleteFile(strNetworkLogonLogPath & "online\" & strLogonLogFile)
'Else wscript.echo "Remote"
End If

WScript.Quit
