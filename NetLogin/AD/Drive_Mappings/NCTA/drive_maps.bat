if exist g: net use g: /delete
net use g: \\hades\shared /persistent:no

if exist n: net use n: /delete
net use n: \\hades\data /persistent:no

if exist s: net use s: /delete
net use s: \\hades\scanners-repository /persistent:no

if exist P: net use p: /delete
if exist \\hades\users\%username%\ goto HADES
if exist \\files-vii\users\%username%\ goto FILES7
Goto End


:FILES7
net use p: \\files-vii\users\%username%
goto end

:HADES
net use p: \\hades\users\%username%

:end
