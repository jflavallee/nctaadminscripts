' Get operating system so XP maps the drives correctly
Dim WSHShell, compname, objGroup, osVer
Set WSHShell = WScript.CreateObject("WScript.Shell")
' Get local computer name to figure OS version below
compname = WSHShell.ExpandEnvironmentStrings("%COMPUTERNAME%")
Set objGroup = GetObject("WinNT://" & compname & ",computer")
osVer = objGroup.OperatingSystemVersion

'This next section checks the machines IP address to determine if it is offsite (i.e., at the Cable Show)
strSite="ncta"
strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")

Set IPConfigSet = objWMIService.ExecQuery _
    ("Select * from Win32_NetworkAdapterConfiguration Where IPEnabled=TRUE")
 


If osVer >= "6.0" Then
wshShell.LogEvent 0,"Logon Script is running."
	For Each IPConfig in IPConfigSet	
		If Not IsNull(IPConfig.IPAddress) Then 
        		For i=LBound(IPConfig.IPAddress) to UBound(IPConfig.IPAddress)
        		If instr(IPConfig.IPAddress(i), "172.25") then
					wshShell.Run "cscript \\ncta.com\netlogon\drive_mappings\accounting\create_task.wsf \\ncta.com\NETLOGON\drive_mappings\accounting\drive_maps.bat"
					Else
					wscript.quit
				End If
				Next
		End If
        Next
End If
