'Need to get rid of On Error Resume Next after plenty of testing
On Error Resume Next
Dim objShell, objNetwork, objUser, objCurrentUser, objLogonLogFile
Dim colGroupList,colVolatileEnvVars
Dim strGroup, strGroupPath, strAllGroups, strOutlookTag, strUserName, strLogonLogFile, strLocalLogonLogPath, strNetworkLogonLogPath
Dim strComputerName, strDesktopMgr, strComputer, strSite, strUpdateFile, strUpdateFileVer, strSophosALMon, strReader9, strReader9TestLog, strLogComment, intLogTest
Dim strUserID, strUserHome

strNetworkLogonLogPath = "\\dc-ii\logon_logs\"
intLogTest = 0

Set objShell = WScript.CreateObject("WScript.Shell")
Set objNetwork = CreateObject("WScript.Network")
Set objUser = CreateObject("ADSystemInfo")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set colVolatileEnvVars = objShell.Environment("Volatile")

'Get Computer Name, User info and Group Membership
strComputerName = objNetwork.ComputerName
Set objCurrentUser = GetObject("LDAP://" & objUser.UserName)
strUserName = objCurrentUser.Get("name")
strUserID = objCurrentUser.Get("SamAccountName")
colGroupList = objCurrentUser.GetEx("MemberOf")

'Create an application log event showing that the user is logging on
objShell.LogEvent 0,"User " & strUserName & " is logging on..."

For Each strGroup in colGroupList
	strGroupPath = "LDAP://" & strGroup
	Set objGroup = GetObject(strGroupPath)
	
	strAllGroups = strAllGroups & " " & objGroup.CN
'	wscript.echo objGroup.cn
Next

'This next section checks the machines IP address to determine if it is offsite (i.e., at the Cable Show)
strSite="ncta"
strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")

Set IPConfigSet = objWMIService.ExecQuery _
    ("Select * from Win32_NetworkAdapterConfiguration Where IPEnabled=TRUE")
 
For Each IPConfig in IPConfigSet
    If Not IsNull(IPConfig.IPAddress) Then 
        For i=LBound(IPConfig.IPAddress) to UBound(IPConfig.IPAddress)
        If instr(IPConfig.IPAddress(i), "172.27") then
	strSite="remote"
	End If
        Next
    End If
Next

'Map all normal drives  except if logging onto Files-vii; use Site test as hack
'The reason is that Files-vii already maps to the N: and P: drives; mapping again causes no problem except
'for an error in the event logs.
If objFSO.FileExists("c:\files-vii.id") Then strSite="ncta_files-vii"

strUserHome = "\\ncta.com\storage\users\" & strUserID
strOldUserHome = "\\files-vii\users\" & strUserID

If strSite="ncta" then
	If objFSO.DriveExists ("G:") then
		objNetwork.RemoveNetworkDrive "G:", True, True
	End If	
	objNetwork.MapNetworkDrive "G:" , "\\ncta.com\storage\shared"
	
	If objFSO.DriveExists ("N:") then
		objNetwork.RemoveNetworkDrive "N:", True, True
	End If
	objNetwork.MapNetworkDrive "N:" , "\\ncta.com\storage\data"
	
	If objFSO.DriveExists ("S:") then
		objNetwork.RemoveNetworkDrive "S:", True, True
	End If
	objNetwork.MapNetworkDrive "S:" , "\\ncta.com\storage\scanners-repository"
	
	If objFSO.DriveExists ("P:") then
		objNetwork.RemoveNetworkDrive "P:", True, True
	End IF
	If objFSO.FolderExists(strUserHome) then
		objNetwork.MapNetworkDrive "P:" , strUserHome
		ElseIf objFSO.FolderExists(strOldUserHome) then
			objNetwork.MapNetworkDrive "P:" , strOldUserHome
	End If	
	
	ElseIf strSite="remote" Then
	objNetwork.MapNetworkDrive "N:" , "\\gypsy-3\data"
End If

'Group Drive Mappings
If InStr(strAllGroups, "Accounting") Then
'	objNetwork.MapNetworkDrive "O:" , "\\navision-2\data"
	objNetwork.MapNetworkDrive "O:" , "\\files-vii\data"
End If

'Logon Tests
strLogComment = "Acrobat Reader 9 is *not* installed."
strReader9 = "C:\Program Files\Adobe\Reader 9.0\Reader\AcroRd32.exe"
strReader9x64 = "C:\Program Files (x86)\Adobe\Reader 9.0\Reader\AcroRd32.exe"
If objFSO.FileExists(strReader9) Then
	strLogComment = "Acrobat Reader 9 is still installed!"
	intLogTest = 1
	Else If objFSO.FileExists(strReader9x64) Then
		strLogComment = "Acrobat Reader 9 is still installed!"
		intLogTest = 1
	End If
End If

'wscript.echo "Log Test = " & intLogTest
'Logon Log
'This section creates or updates a file that shows login/logout date and time. The filenames are USERNAME_on_COMPUTERNAME.log
'A related section is found in the logout script.

strLogonLogFile = strUserName & "_On_" & strComputerName & ".log"
If objFSO.FileExists(strNetworkLogonLogPath & strLogonLogFile) Then
'no action needed
	Else Set objLogonLogFile = objFSO.CreateTextFile(strNetworkLogonLogPath & strLogonLogFile)
End If
Set objLogonLogFile = objFSO.OpenTextFile(strNetworkLogonLogPath & strLogonLogFile, 8)
objLogonLogFile.WriteLine "In, " & date & ", " & time & "; " & strLogComment
objLogonLogFile.close
objFSO.CopyFile strNetworkLogonLogPath & strLogonLogFile , strNetworkLogonLogPath & "online\"

strReader9TestLog = strNetworkLogonLogPath & "\reader9\" & strComputerName & ".txt"
If intLogTest = 1 Then
	objLogonLogFile = objFSO.CreateTextFile(strReader9TestLog)
'	The following two lines add an entry to the Application Event Log, which not very useful
'	objShell.LogEvent 0,"PC " & strComputerName & " has Reader 9 installed."
'	Else objShell.LogEvent 0,"PC " & strComputerName & " does not have Reader 9 installed."
Else If objFSO.FileExists(strReader9TestLog) Then
'	wscript.echo "File ought to go!"
	objFSO.DeleteFile(strReader9TestLog)
	End If
End if

WScript.Quit
