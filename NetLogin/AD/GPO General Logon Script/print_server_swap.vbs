'DAG Notes:
'Mistakes made--No need for a SKIP option in the Pandorra array. Andorra printers could be
'removed without having a related Pandora printer added. When one of those printers comes up
'in the collection, the Andorra array would be cylced through without any action occurring.
'Also, the Pandora array could have just been the printer name without the server name (e.g., aa-hp4250)
'which would have eliminated the need for that Mid() function. Handling all those back-slashes proved
'to be harder than I had originally thought.

'Without further ado, the script...

On Error Resume Next

strComputer = "."
Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set objShell = WScript.CreateObject("WScript.Shell")
Set objNetwork = CreateObject("WScript.Network")
Set objFSO = CreateObject("Scripting.FileSystemObject")
DPmsg = "The default printer has not been changed."

If objFSO.FileExists("c:\server.id") Then
	objShell.LogEvent 0,"Quitting the Printer Swap script without running."
	WScript.Quit
	End If

Dim NewPrinters(31)
Dim printers(31,2)
printers(0,0)="\\andorra\aa-hp4250"
printers(1,0)="\\andorra\aa-phaser8560"
printers(2,0)="\\andorra\aa-wc5225"
printers(3,0)="\\andorra\acct-hp4250"
printers(4,0)="\\andorra\Admin-Phaser8550"
printers(5,0)="\\andorra\CEO-HP4250"
printers(6,0)="\\andorra\cic-hp4100"
printers(7,0)="\\andorra\cic-phaser6200"
printers(8,0)="\\andorra\cic-phaser8650"
printers(9,0)="\\andorra\cpa-dc432st"
printers(10,0)="\\andorra\cpa-hp4250"
printers(11,0)="\\andorra\cpa-wc7655"
printers(12,0)="\\andorra\eo-phaser8400"
printers(13,0)="\\andorra\eo-phaser8550"
printers(14,0)="\\andorra\eo-wc128"
printers(15,0)="\\andorra\evp-hp4250"
printers(16,0)="\\andorra\GR-HP4250"
printers(17,0)="\\andorra\gr-phaser8550"
printers(18,0)="\\andorra\GR-WC5225"
printers(19,0)="\\andorra\ia-hp4250"
printers(20,0)="\\andorra\ia-hp5550"
printers(21,0)="\\andorra\ia-wc265"
printers(22,0)="\\andorra\it-hp4050"
printers(23,0)="\\andorra\Legal-HP4100"
printers(24,0)="\\andorra\legal-phaser8400"
printers(25,0)="\\andorra\legal-wc5632"
printers(26,0)="\\andorra\Mailroom-4110"
printers(27,0)="\\andorra\pac-hp2200"
printers(28,0)="\\andorra\pac-hp4240"
printers(29,0)="\\andorra\PNP-HP4015"
printers(30,0)="\\andorra\scitech-hp4050"

printers(0,1)="\\pandora\aa-hp4250"
printers(1,1)="\\pandora\aa-phaser8560"
printers(2,1)="\\pandora\aa-wc5225"
printers(3,1)="\\pandora\acct-hp4250"
printers(4,1)="\\pandora\Admin-Phaser8550"
printers(5,1)="\\pandora\CEO-HP4250"
printers(6,1)="\\pandora\scitech-hp4015"
printers(7,1)="\\pandora\cic-phaser8560"
printers(8,1)="\\pandora\cic-phaser8560"
printers(9,1)="\\pandora\cpa-wc7655"
printers(10,1)="\\pandora\cpa-hp4250"
printers(11,1)="\\pandora\cpa-wc7655"
printers(12,1)="\\pandora\eo-phaser8550"
printers(13,1)="\\pandora\eo-phaser8550"
printers(14,1)="\\pandora\eo-wc128"
printers(15,1)="\\pandora\evp-hp4250"
printers(16,1)="\\pandora\GR-HP4250"
printers(17,1)="\\pandora\gr-phaser8550"
printers(18,1)="\\pandora\GR-WC5225"
printers(19,1)="\\pandora\ia-hp4250"
printers(20,1)="\\pandora\ia-hp5550"
printers(21,1)="\\pandora\ia-wc265"
printers(22,1)="\\pandora\it-hp4050"
printers(23,1)="\\pandora\Legal-HP4100"
printers(24,1)="\\pandora\legal-phaser8400"
printers(25,1)="\\pandora\legal-wc5632"
printers(26,1)="skip"
printers(27,1)="\\pandora\PNP-HP4015"
printers(28,1)="\\pandora\pac-hp4240"
printers(29,1)="\\pandora\PNP-HP4015"
printers(30,1)="\\pandora\scitech-hp4015"

    Set colInstalledPrinters =  objWMIService.ExecQuery _
        ("Select * from Win32_Printer where name > '\\\\andorr' and name < '\\\\andorrb'")

For Each objPrinter in colInstalledPrinters
	PN= objprinter.name
'	wscript.echo PN
		If objPrinter.default = -1 then
		OldDP = objprinter.name
'		wscript.echo "The current default printer is " & OldDP & "."
'		objShell.LogEvent 0,"The current default printer is " & OldDP & "."
		End If

	For x = 0 to 30
	AP = printers(x,0)
	PP = printers(x,1)
	If AP = PN Then
		IF PP <> "skip" then 
'		wscript.echo "Adding " & PP & "."
		objNetwork.AddWindowsPrinterConnection PP
		objShell.LogEvent 0,"Adding " & PP & "."
		End If
	End If
	If AP = OldDP Then 
		If PP = "skip" Then
		NewDP = "PDFCreator"
		NewDPname = NewDP
'		wscript.echo "New Default is to be " & NewDPname
		Else
		NewDP = mid(PP,11)
		NewDPname = "\\pandora\" & NewDP
		NewDP = "\\\\pandora\\" & NewDP
		End If
		DPmsg = "Setting " & NewDPname & " as the new default printer."
		End If	
	Next
Next

'Cycle through the collection again to delete Andorra printers.
'Note that the collection hasn't been re-enumerated, and therefore the 
'Pandora printers won't be removed (i.e., the collection still only contains
'the original members, Andorra printers only).
For Each objPrinter in colInstalledPrinters	
	PN=objprinter.name		
	objNetwork.RemovePrinterConnection PN
	objShell.LogEvent 0,"Removing " & PN & "."
'	wscript.echo "Removing " & PN & "."	
Next

xyz = "Select * from Win32_Printer where name = " & NewDP
Set colInstalledPrinters2 =  objWMIService.ExecQuery("Select * from Win32_Printer where name ='" & NewDP & "'")
For Each objPrinter2 in colInstalledPrinters2
'wscript.echo objPrinter2.name
    objPrinter2.SetDefaultPrinter()
Next
'	wscript.echo DPmsg
	objShell.LogEvent 0,DPmsg