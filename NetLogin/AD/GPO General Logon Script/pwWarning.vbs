'PwWarning.vbs

'========================================
' First, get the domain policy.
'========================================
Dim oDomain, oUser, maxPwdAge, numDays, WSHShell, warningDays, CompName, objGroup, osVer, TimeLeft

Set WSHShell = WScript.CreateObject("WScript.Shell")
compname = WSHShell.ExpandEnvironmentStrings("%COMPUTERNAME%")
Set objGroup = GetObject("WinNT://" & compname & ",computer")
osVer = objGroup.OperatingSystemVersion

If osVer >= "6.0" Then
warningDays = 14

Set LoginInfo = CreateObject("ADSystemInfo") 
Set objUser = GetObject("LDAP://" & LoginInfo.UserName & "") 
strDomainDN = UCase(LoginInfo.DomainDNSName) 
strUserDN = LoginInfo.UserName


Set oDomain = GetObject("LDAP://" & strDomainDN)
Set maxPwdAge = oDomain.Get("maxPwdAge")

'========================================
' Calculate the number of days that are
' held in this value.
'========================================
numDays = CCur((maxPwdAge.HighPart * 2 ^ 32) + _
maxPwdAge.LowPart) / CCur(-864000000000)
'WScript.Echo "Maximum Password Age: " & numDays

'========================================
' Determine the last time that the user
' changed his or her password.
'========================================
Set oUser = GetObject("LDAP://" & strUserDN)

'========================================
' Add the number of days to the last time
' the password was set.
'========================================
whenPasswordExpires = DateAdd("d", numDays, oUser.PasswordLastChanged)
fromDate = Date
daysLeft = DateDiff("d",fromDate,whenPasswordExpires)
If daysleft <= 1 then
TimeLeft="in less than 24 hours!!!!"
Else
If daysleft <=4 then
TimeLeft="in a matter of days!!!"
Else
if daysleft > 7 then
TimeLeft="in less than two weeks!"
else
if daysleft <= 7 then
TimeLeft="in less than one week!!"
End If
End If
End If
End If

'WScript.Echo "Password Last Changed: " & oUser.PasswordLastChanged

if (daysLeft < warningDays) and (daysLeft > -1) then
Msgbox "Your password expires " & TimeLeft & chr(13) & chr(13) & "The expiration date/time is " & whenPasswordExpires & "." & chr(13) & chr(13) & "Once you are logged in, press CTRL-ALT-DEL and" & chr(13) & "select the 'Change a password' option." & chr(13) & chr(13) & "Don't delay, change it today!", 0, "!!! Password Expiration Warning !!!"
End if

'========================================
' Clean up.
'========================================
Set oUser = Nothing
Set maxPwdAge = Nothing
Set oDomain = Nothing 
End If