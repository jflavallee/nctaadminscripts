'This script is for testing purposes... It watches the process list and addes entries to the Application Event Log as processes start and end.
'Mostly, we're watching to see if DesktopMgr ends on its own.

Set objShell = WScript.CreateObject("WScript.Shell")
strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colMonitoredProcesses = objWMIService. _
    ExecNotificationQuery("select * from __instancedeletionevent " _ 
            & "within 1 where TargetInstance isa 'Win32_Process'")
i = 0

'Watch until x number of processes shut down.
Do While i < 50
    Set objLatestProcess = colMonitoredProcesses.NextEvent
    objShell.LogEvent 0,"Process terminated: " & objLatestProcess.TargetInstance.Name
	i = i + 1
Loop
objShell.LogEvent 0,"end_proc.vbs has ended."