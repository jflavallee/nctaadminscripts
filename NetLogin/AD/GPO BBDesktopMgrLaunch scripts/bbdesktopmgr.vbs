On Error Resume Next
Dim objShell, objNetwork, objUser, objFSO, objCurrentUser, objReg, objWMIService, objStartup, objProcess, objConfig
Dim colGroupList,colVolatileEnvVars, colProcessList
Dim strGroup, strGroupPath, strAllGroups, strOutlookTag, strUserName, strLogonLogFile, strLocalLogonLogPath, strNetworkLogonLogPath
Dim strComputerName, strDesktopMgr, strDesktopMgrX86, strDesktopMgrCheck, strDesktopMgrPath, strComputer, strSite, strUpdateFile, strUpdateFileVer, strSophosALMon
Dim strKeyPath1, strValueName1, strKeyPath2, strValueName2

Set objShell = WScript.CreateObject("WScript.Shell")
Set objNetwork = CreateObject("WScript.Network")
Set objUser = CreateObject("ADSystemInfo")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set colVolatileEnvVars = objShell.Environment("Volatile")
strDesktopMgrPath = "X"
strDesktopMgrCheck = "0"
'wscript.echo strDesktopMgrPath

wscript.sleep 5000

'Blackberry startup
'Apply Auto Syncronize and Hide When Minimized settings to user's registry.
Const HKEY_CURRENT_USER = &H80000001
Const SW_MINIMIZE = 6
Const HIDDEN_WINDOW = 12
strComputer = "."
Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\default:StdRegProv")
strKeyPath1 = "Software\Research In Motion\BlackBerry\Synchronize"
strValueName1 = "Parameters"
strKeyPath2 = "Software\Research In Motion\BlackBerry\Manager\Settings"
strValueName2 = "HideWhenMinimized"


strDesktopMgr = "C:\Program Files\Research In Motion\BlackBerry\DesktopMgr.exe"
strDesktopMgrX86 = "C:\Program Files (x86)\Research In Motion\BlackBerry\DesktopMgr.exe"
'Check to see if Desktop Manager is even installed on the PC
If objFSO.FileExists(strDesktopMgrX86) Then
	strDesktopMgrPath = strDesktopMgrX86
		Else If objFSO.FileExists(strDesktopMgr) Then	
	'If objFSO.FileExists(strDesktopMgr) Then
	strDesktopMgrPath = strDesktopMgr
	End If
End If
'Wscript.echo strDesktopMgrPath

If strDesktopMgrPath <> "X" Then
	objShell.LogEvent 0,"Logon Script Blackberry Sync Step 1: Desktop Manager exists on this PC, and will run if Step 2 completes."
	objReg.SetDWORDValue HKEY_CURRENT_USER,strKeyPath1,strValueName1,&H0000003f
	objReg.SetDWORDValue HKEY_CURRENT_USER,strKeyPath2,strValueName2,&H00000001

'Check to see if the user has logged into Outlook yet. If not, don't bother launching BB Desktop Mgr
	strOutlookTag = colVolatileEnvVars("appdata") & "\microsoft\outlook\default user profile.srs"
		If objFSO.FileExists(strOutlookTag) Then
			objShell.LogEvent 0,"Logon Script Blackberry Sync Step 2: The correct SRS file exists on this PC. Desktop Manager should launch immediately..."
			'Call Subroutine
			LaunchDesktopMgr
			Else objShell.LogEvent 1,"Logon Script Blackberry Sync Step 2: The correct SRS file DOES NOT exist on this PC. Desktop Manager won't run."
		End If
Else objShell.LogEvent 1,"Logon Script Blackberry Sync Step 1: Desktop Manager DOES NOT exist on this PC and therefore will not run."
End If

'Verify Desktop Manager is still running; if not, relaunch it damnit!
'Wait for x number of milliseconds before checking the process list.
wscript.sleep 30000
intBB = 0
Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process")
For Each objProcess in colProcessList
	If objProcess.Name = "DesktopMgr.exe" Then 
	intBB = 1
	End If
Next
If intBB = 0 Then
	objShell.LogEvent 0,"Desktop Manager being relaunched..."
	LaunchDesktopMgr
End If	

objShell.LogEvent 0,"The Blackberry Launch Script is shutting down."
WScript.Quit	

'Desktop Manager can get launched from one of two places in the script, so a subroutine is in order...
Sub LaunchDesktopMgr
Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
			Set objStartup = objWMIService.Get("Win32_ProcessStartup")
			Set objConfig = objStartup.SpawnInstance_
objConfig.ShowWindow = HIDDEN_WINDOW
			Set objProcess = GetObject("winmgmts:root\cimv2:Win32_Process")
			errReturn = objProcess.Create (strDesktopMgrPath, null, objConfig, intProcessID)

			If errReturn = 0 Then
				objShell.LogEvent 0,"Logon Script Blackberry Sync. Desktop Manager was started with a process ID of " & intProcessID & "."
				Else
				objShell.LogEvent 1,"Logon Script Blackberry Sync. Desktop Manager could not be started due to error " & Error & "."
			End If
End Sub
