This repository contains the administrative scripts used by NCTA IT to administer the network.

The basic organization is:

-NetLogin : this folder is for the userland login scripts
-ServersAndApps : this folder is for servers and application scripts
-NetBackbone : this folder is for cisco and other router configuration files